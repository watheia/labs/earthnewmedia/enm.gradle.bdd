/*
 * 
 */
package earthnewmedia.gradle.bdd

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class BddExtension {
    def reportDirectory = 'build/reports/bdd'
    String junitVersion = "4.12"
    String hamcrestVersion = "2.2"
    String serenityVersion = "2.2.1"
    String serenityCucumberVersion = "2.2.0"
    String jsonSchemaVersion = "3.3.0"
}