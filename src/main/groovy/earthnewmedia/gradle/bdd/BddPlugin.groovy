/*
 * 
 */
package earthnewmedia.gradle.bdd

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.BasePlugin
import org.gradle.api.plugins.GroovyPlugin
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.testing.Test
import org.gradle.api.Task

import net.serenitybdd.plugins.gradle.SerenityPlugin
import net.serenitybdd.plugins.gradle.SerenityPluginExtension

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class BddPlugin implements Plugin<Project> {

    static final String DEPENDENCY_GROUP = "implementation"

    static Set<String> testdeps(final BddExtension opts) {
        return [
            "junit:junit:${opts.junitVersion}",
            "org.hamcrest:hamcrest:${opts.hamcrestVersion}",
            "net.serenity-bdd:serenity-core:${opts.serenityVersion}",
            "net.serenity-bdd:serenity-junit:${opts.serenityVersion}",
            "net.serenity-bdd:serenity-ensure:${opts.serenityVersion}",
            "net.serenity-bdd:serenity-screenplay:${opts.serenityVersion}",
            "net.serenity-bdd:serenity-screenplay-webdriver:${opts.serenityVersion}",
            "net.serenity-bdd:serenity-screenplay-rest:${opts.serenityVersion}",
            "net.serenity-bdd:serenity-cucumber5:${opts.serenityCucumberVersion}",
            "io.rest-assured:json-schema-validator:${opts.jsonSchemaVersion}"
        ]
    }

    @Override
    void apply(Project project) {

        // Apply required plugins
        project.getPluginManager().apply(BasePlugin)
        project.getPluginManager().apply(JavaPlugin)
        project.getPluginManager().apply(GroovyPlugin)
        project.getPluginManager().apply(SerenityPlugin)

        // Apply required deendencies according to specified version
        project.extensions.create('bdd', BddExtension)
        final BddExtension bdd = project.extensions.getByType(BddExtension)
        testdeps(bdd).each {
            project.getDependencies().add(DEPENDENCY_GROUP, it)
        }

        final Task aggregate = project.tasks.getByName('aggregate')

        final Test test = project.tasks.getByName('test')
        test.testLogging.showStandardStreams = true
        test.systemProperties = System.properties
        test.ignoreFailures = true
        test.finalizedBy(aggregate)
        
        final SerenityPluginExtension serenity = project.extensions.getByType(SerenityPluginExtension)
        serenity.outputDirectory = bdd.reportDirectory
    }
}
